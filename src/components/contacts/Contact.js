import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types'
import {Consumer} from '../../context'
import axios from 'axios'

class Contact extends Component{

  state = {
    showContactInfo: false
  }

  onShowClick = e => {
    this.setState({
      showContactInfo: !this.state.showContactInfo
    })
  }

  onDeleteClick = (id, dispatch) => {
    axios.delete(`https://jsonplaceholder.typicode.com/users/${id}`).then(res => dispatch({type: 'DELETE_CONTACT', payload: id}))
    
  }

  render(){
    const {id, name, email, phone} = this.props.contact
    const {showContactInfo} = this.state
    return(
      <Consumer>
        {value => {
          const {dispatch} = value
          return(
            <div className="card card-body mb-3">
              <h4>{name} 
              <i 
                onClick={this.onShowClick} className="fa fa-caret-down" 
                style={{cursor: 'pointer', marginLeft: 10}} 
              />
              <i 
                onClick={this.onDeleteClick.bind(this,id,dispatch)}
                className="fa fa-minus-square" style={{cursor: 'pointer', float: 'right', color: 'red'}}
              />
              <Link to={`contact/edit/${id}`}>
                <i 
                  className="fa fa-pencil" 
                  style={{
                    cursor: 'pointer',
                    color: 'black',
                    float: 'right',
                    marginRight: '1rem'
                    }}>
                </i>
              </Link>
              </h4>
              {showContactInfo ? (
              <ul className="list-group">
                <li className="list-group-item">Email: {email}</li>
                <li className="list-group-item">Phone: {phone}</li>
              </ul>
              ) : null }
        
            </div>
          )
        }}
      </Consumer>
    )
  }
}


export default Contact 