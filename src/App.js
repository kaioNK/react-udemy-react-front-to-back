import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Contacts from './components/contacts/Contacts'
import Header from './components/layout/Header'
import AddContact from './components/contacts/AddContact'
import About from './components/pages/About'
import NotFound from './components/pages/NotFound'
import Test from './components/test/Test'
import EditContact from './components/contacts/EditContact'

import {Provider} from './context'

import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';

class App extends Component{
 
  render(){

    //const name = "Kaio"

    //return só retorna JSX
    return(
      <Provider>
        <Router>
        <div className="App">
          <Header branding="Contact Manager" />
          <div className="container">
            <Switch>
              <Route exact path="/" component={Contacts} />
              <Route exact path="/contact/add" component={AddContact} />
              <Route exact path="/contact/edit/:id" component={EditContact} />
              <Route exact path="/about" component={About} />
              <Route exact path="/not_found" component={NotFound} />
              <Route exact path="/test" component={Test} />
            </Switch>
          </div> 
        </div>
        </Router>
      </Provider>
    )
  }
}

export default App;
